import Vue from 'vue'
import Router from 'vue-router'
import Get from '@/components/Get'
import Post from '@/components/Post'
import Delete from '@/components/Delete'
import Put from '@/components/Put'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/get',
      name: 'get',
      component: Get
    },
    {
      path: '/post',
      name: 'post',
      component: Post
    },
    {
      path: '/delete',
      name: 'delete',
      component: Delete
    },
    {
      path: '/put',
      name: 'put',
      component: Put
    }

  ]
})
